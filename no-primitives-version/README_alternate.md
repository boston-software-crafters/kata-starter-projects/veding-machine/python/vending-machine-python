#Accept Coins
------------
  
User requirement:
_As a vendor_  
_I want a vending machine that accepts coins_  
_So that I can collect money from the customer_  

The vending machine will accept valid coins (nickels, dimes, and quarters) ...

Tests:
- You can create a Vending machine object.
- When vendingMache.depositCoin(coin: Coin) is called, vendingMachine.depositedCounts will equal an array 
with a single element.
