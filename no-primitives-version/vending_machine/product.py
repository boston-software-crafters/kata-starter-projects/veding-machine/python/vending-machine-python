from dataclasses import dataclass
from enum import Enum, auto


class Product(Enum):
    SODA = auto()
    CHIPS = auto()
    CANDY = auto()
    DONUT = auto()
    CAKE = auto()
    PIZZA = auto()

    def __lt__(self, other):
        return self.name < other.name


# frozen makes these hashable, and thus can be keys for hash maps (dictionaries)
# order makes these sortable
@dataclass(order=True, frozen=True)
class ProductKey:
    column: str
    row: int
