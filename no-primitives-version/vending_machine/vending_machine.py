from dataclasses import dataclass
from typing import Iterable

from vending_machine.money import Coin, Dollar
from vending_machine.product import ProductKey, Product
from vending_machine.display import AllProductsDisplay, MainDisplay


class VendingMachine(object):
    def __init__(self, current_value: Dollar = Dollar(0.0)):
        self.current_value = current_value

    def display_value(self) -> MainDisplay:
        raise NotImplementedError("Show state")

    def display_products(self) -> AllProductsDisplay:
        raise NotImplementedError

    def insert_coin(self, coin: Coin) -> None:
        raise NotImplementedError("make some state to hold coins")

    def get_change(self) -> "ChangeBin":
        raise NotImplementedError

    def select_product(self, key: ProductKey) -> None:
        raise NotImplementedError

    def get_products(self) -> "ProductBin":
        raise NotImplementedError


@dataclass
class ChangeBin:
    """this is a private variable"""

    _coins: Iterable[Coin]

    def __iter__(self):
        """You can now for loop over ChangeSlot, `list(ChangeSlot)` and `sorted(ChangeSlot)`"""
        return (coin for coin in self._coins)

    def __eq__(self, other):
        """equality tests for content and NOT order"""
        if not isinstance(other, ChangeBin):
            return False
        return sorted(self) == sorted(other)


@dataclass
class ProductBin:
    _products: Iterable[Product]

    def __iter__(self):
        return (product for product in self._products)

    def __eq__(self, other):
        """equality tests for content and NOT order"""
        if not isinstance(other, ProductBin):
            return False
        return sorted(self) == sorted(other)
