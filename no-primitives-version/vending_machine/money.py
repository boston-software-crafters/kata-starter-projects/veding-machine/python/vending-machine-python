from dataclasses import dataclass
from enum import Enum, auto


class Coin(Enum):
    NICKEL = auto()
    DIME = auto()
    QUARTER = auto()

    def __lt__(self, other):
        return self.name < other.name


@dataclass(frozen=True, order=True)
class Dollar:
    value: float

    def __str__(self):
        """Useful python formatting here:
        https://docs.python.org/3/library/string.html#format-specification-mini-language"""
        raise NotImplementedError

    @classmethod
    def from_coin(cls, coin: Coin) -> "Dollar":
        raise NotImplementedError

    def plus(self, other: "Dollar") -> "Dollar":
        return Dollar(self.value + other.value)

    def minus(self, other: "Dollar") -> "Dollar":
        return Dollar(self.value - other.value)


@dataclass(frozen=True, order=True)
class Cent:
    value: int

    def __str__(self):
        raise NotImplementedError
