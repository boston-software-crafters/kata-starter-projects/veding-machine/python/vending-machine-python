# Virtual Vending Machine

This was taken from Guy Royse's github at https://github.com/guyroyse/vending-machine-kata


In this exercise you will build the brains of a vending machine.  It will accept money, make change, maintain
inventory, and dispense products.  All the things that you might expect a vending machine to accomplish.

Several basic classes have been created for you with the minimum of implementation.  This is 
meant to help lay out a skeleton for the exercise and make clear return types for functions.  **These are
incomplete data structures and will probably need to be updated throughout the exercise.  They are there
to _aid_ you.  If they are not useful, don't use them.**

For python, this means
heavy reliance on the `dataclass` wrapper, which creates a simple data object and takes care of boilerplate
like `__init__`, `__eq__`, `__lt__` , etc... This has an added advantage that nested dataclasses are easy to 
convert to JSON serializable objects with`dataclasses.asdict`.

# Requirements

These requirements have several method and class names in them.  They are not set in stone.  If you don't like
them, use other consistent naming conventions.

## A Map of the Basic State

![state map](./vending-machine.png)

## Display

### 1 - Display INSERT COIN

Create a vending machine object.  When you call its **display value** method, it returns a MainDisplay 
with a message field of: "INSERT COIN"

### 2 - Display State

When your vending machine has a monetary value stored,  and you call its **display value** method, it returns a MainDisplay
with a message field of the USD of that value:  ex: "$3.25"

## Coins

A `Coin` can be a QUARTER, NICKEL or DIME.  When they are inserted into the vending machine, their 
dollar value must be updated.

### 3 - Change Bin

When you call the vending machine's **get change** method it returns an empty ChangeBin.

### 5 - Insert Coins that update VendingMachine

When you insert a Coin that is a NICKEL, DIME or QUARTER, it changes the stored monetary value of the vending machine.

### 6 - Cancel

When you **cancel**, your money gets put in the change bin _and_ the vending machine's value is set to zero.
Now, when you call **get change**, the change bin will contain all your coins.

### 7 - Empty Change Bin

When you **empty change bin** it now has no change in it

## Products

### 8 - Display Products

When you call **display products**, it returns a **AllProductsDisplay**. Each **SingleProductDisplay** can give the 
following information:

- name: the display name of the product
- price: the price of the product (ex: "$3.25")
- key: a ProductKey for selecting the product (ex: ProductKey("A", 1) for soda, ProductKey("B", 2) for chips) 

### 9 - Product Bin

When you call **get product** it returns an empty ProductBin

If a product has been selected, it returns that Product.

### 10 - Empty Bin

When you call **empty product bin**, the product bin is now in an empty state

### 11 - Select Product

1. When you **select product** it is put in the ProductBin
2. When you **select product** _and_ there is enough value in the vending machine, it is put in the product bin
3. When you **select product** _and_ there is enough value in the vending machine, the correct product is 
   put into the bin **and** the vending machine value is adjusted

### 12 - Get Change

Depending on how you implemented **cancel**, you may need adjust how you return change

_Now_ , when you select a product, and you have enough value in the vending machine, it ...

1. puts the product in the bin
2. puts the correct change in the ChangeBin
3. adjusts the vending machine value to zero


## Inventory

This will require some refactoring.  The vending machine now keeps track of the coins it has, and the products it has.
There is now an extra field to the **display products** that will tell you if it's **out of stock**.  The
**MainDisplay** will now include a string message, and a status indication of whether you need exact change.
The vending machine need to behave accordingly

### 13 - Update Display Product

This will now show whether or not the product is out of stock.

- name: the display name of the product
- price: the price of the product
- key: a string for selecting the product (ex: "A1" for soda, "A2" for chips, "B1" for candy, "B2" for cookies) 
- status: an Enum of OUT_OF_STOCK, IN_STOCK **or** out_of_stock: True/False. 
  The status approach gives you more leeway moving forward. Perhaps you want more inventory statuses.  Maybe
  someone else needs to query the machine to find out about LOW_STOCK.

### 14 - You cannot select OUT_OF_STOCK items

what it says

### 15 - Update Inventory

Your Vending Machine now needs to own an Inventory.  You can query this for product.  When you select a product,
it reduces inventory.  When a product is zero, it's OUT_OF_STOCK.

### 16 - Exact Change Display

When your vending machine is in Exact Change Mode, it will display EXACT CHANGE ONLY instead of INSERT COIN.

### 16 - Exact Change Mode Behavior

When the vending machine is in exact change mode, you can _only_ purchase items with exact change.

### 17 - Coin Inventory

Your Vending Machine now needs to own a Coin Inventory.  When users **insert coin**, it adds to the Coin Inventory.
When a Coin goes into the Change Bin, it removes from the Coin Inventory.  When there are not enough of the right
kinds of coins to provide change for any possible purchase, the Vending Machine goes into Exact Change Mode.

