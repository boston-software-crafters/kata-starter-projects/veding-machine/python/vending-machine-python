import pytest
from vending_machine.money import Coin, Dollar
from vending_machine.vending_machine import VendingMachine, ChangeBin, MainDisplay


class TestVendingMachine:
    @pytest.mark.skip(reason="#1 not implemented")
    def test_empty_vending_machine_displays_insert_coin(self):
        machine = VendingMachine(Dollar(0.0))
        assert machine.display_value() == MainDisplay("INSERT COIN")

    @pytest.mark.skip(reason="#2 not implemented")
    def test_non_empty_vending_machine_displays_dollar_value(self):
        machine = VendingMachine(Dollar(1.23))
        assert machine.display_value() == MainDisplay("$1.23")

    @pytest.mark.skip(reason="#3 not implemented")
    def test_empty_machine_get_change(self):
        machine = VendingMachine()
        assert machine.get_change() == ChangeBin([])

    @pytest.mark.skip(reason="#4 not implemented")
    @pytest.mark.parametrize("coin", list(Coin))
    def test_insert_coin_goes_to_get_change(self, coin):
        machine = VendingMachine()
        machine.insert_coin(coin)
        assert machine.get_change() == ChangeBin([coin])

    @pytest.mark.skip(reason="#4 not implemented")
    def test_insert_coin_multiple_coins(self):
        machine = VendingMachine()
        first_coin = Coin.DIME
        second_coin = Coin.NICKEL
        machine.insert_coin(first_coin)
        machine.insert_coin(second_coin)
        # ChangeBin equality tests content but NOT order
        assert machine.get_change() == ChangeBin([first_coin, second_coin])

    @pytest.mark.skip(reason="#5 not implemented")
    @pytest.mark.parametrize(
        "coin, display_value",
        [(Coin.DIME, "$0.10"), (Coin.NICKEL, "$0.05"), (Coin.QUARTER, "$0.25")],
    )
    def test_insert_single_accepted_coin(self, coin, display_value):
        machine = VendingMachine()
        machine.insert_coin(coin)

        assert machine.display_value() == MainDisplay(display_value)

    @pytest.mark.skip(reason="#5 not implemented")
    @pytest.mark.parametrize("first_coin", list(Coin))
    @pytest.mark.parametrize("second_coin", list(Coin))
    def test_insert_two_accepted_coins(self, first_coin, second_coin):
        # TODO if you are using Cent, you need to update this test.
        dollars_values = {
            Coin.DIME: Dollar(0.10),
            Coin.NICKEL: Dollar(0.05),
            Coin.QUARTER: Dollar(0.25),
        }
        expected_dollars = dollars_values[first_coin].plus(dollars_values[second_coin])
        expected_string = f"${expected_dollars.value:.2f}"  # format as fixed point to two decimal places

        machine = VendingMachine()
        machine.insert_coin(first_coin)
        machine.insert_coin(second_coin)

        assert machine.display_value() == MainDisplay(expected_string)
