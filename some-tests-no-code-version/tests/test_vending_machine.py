import pytest


class VendingMachine(object):
    pass


class TestVendingMachine:
    @pytest.mark.skip(reason="#1 not implemented")
    def test_new_vending_machine_inits_ith_zero_deposit(self):
        machine = VendingMachine()
        assert machine.coinSlotTotal == 0

    @pytest.mark.skip(reason="#2 not implemented")
    def test_initial_deposit(self):
        machine = VendingMachine()
        machine.deposit(1.25)
        assert machine.coinSlotTotal == 1.25

    @pytest.mark.skip(reason="#3 not implemented")
    def test_three_deposits_accumulate(self):
        machine = VendingMachine()
        machine.deposit(1.25)
        machine.deposit(50)
        machine.depost(25)
        assert machine.coinSlotTotal == 2.00

    def test_provide_change(self):
        machine = VendingMachine()
        machine.deposit(2.00)
        machine.purchase(1.25)
        assert machine.getChange() == 0.75
